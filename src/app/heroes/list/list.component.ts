import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
})
export class ListComponent implements OnInit {
    heroes: string[] = ['IronMan', 'SpiderMan', 'Batman'];
    removedHero: string | null = null;

    constructor() {}

    ngOnInit(): void {}

    removeFirstHero() {
        this.removedHero = this.heroes.shift() || null;
    }
}
