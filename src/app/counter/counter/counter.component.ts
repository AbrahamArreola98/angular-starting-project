import { Component } from '@angular/core';

@Component({
    selector: 'app-counter',
    templateUrl: 'counter.component.html',
})
export class CounterComponent {
    base: number = 5;
    number: number = 0;

    increaseOrDecrease(value: 1 | -1) {
        this.number += value * this.base;
    }
}
