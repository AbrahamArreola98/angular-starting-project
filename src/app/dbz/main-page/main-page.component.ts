import { Component, OnInit } from '@angular/core';

import { DBZCharacter } from '../interfaces/dbz.interface';

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit {
    newCharacter: DBZCharacter = {
        name: 'Trunks',
        powerLvl: 7500,
    };

    constructor() {}

    ngOnInit(): void {}
}
