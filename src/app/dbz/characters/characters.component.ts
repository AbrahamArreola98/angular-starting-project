import { Component } from '@angular/core';
import { DBZCharacter } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
    selector: 'app-characters',
    templateUrl: './characters.component.html',
})
export class CharactersComponent {
    constructor(private dbzService: DbzService) {}

    // @Input()
    // characters: DBZCharacter[] = [];

    get characters(): DBZCharacter[] {
        return this.dbzService.characters;
    }
}
