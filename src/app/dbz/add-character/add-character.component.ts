import { Component, Input, OnInit } from '@angular/core';

import { DBZCharacter } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
    selector: 'app-add-character',
    templateUrl: './add-character.component.html',
})
export class AddCharacterComponent implements OnInit {
    @Input()
    newCharacter: DBZCharacter = {
        name: '',
        powerLvl: 0,
    };

    // @Output()
    // onAddNewCharacter: EventEmitter<DBZCharacter> = new EventEmitter();

    constructor(private dbzService: DbzService) {}

    ngOnInit(): void {}

    addCharacter() {
        if (this.newCharacter.name.trim().length === 0) {
            return;
        }

        // this.onAddNewCharacter.emit(this.newCharacter);
        this.dbzService.addCharacter(this.newCharacter);

        this.newCharacter = {
            name: '',
            powerLvl: 0,
        };
    }
}
