import { Injectable } from '@angular/core';

import { DBZCharacter } from '../interfaces/dbz.interface';

@Injectable()
export class DbzService {
    private _characters: DBZCharacter[] = [
        {
            name: 'Goku',
            powerLvl: 15000,
        },
        {
            name: 'Vegueta',
            powerLvl: 12000,
        },
        {
            name: 'Krillin',
            powerLvl: 2000,
        },
    ];

    get characters(): DBZCharacter[] {
        return [...this._characters];
    }

    constructor() {
        console.log('DBZ Service initialized');
    }

    addCharacter(character: DBZCharacter) {
        this._characters.push(character);
    }
}
