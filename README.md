# Concepts Learned with this project

* Introduction to Angular
* Understanding of the project structure and the files inside of it
* Component Methods
* How the component works
* How to create a component
* `*ngFor`, `*ngIf` directives and `ng-template` tag
* Modules
* Forms Module
* ngModel
* @Input
* @Output
* Services
* Service methods
* How to debug
# Commands used

To run the server and start the application
```bash
npm start
```

or

```bash
ng serve 
```
<hr>

To create a new component
```bash
ng g c <component-name>
```
